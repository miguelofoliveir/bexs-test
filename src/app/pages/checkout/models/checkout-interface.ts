export interface Card {
    number: number;
    name: string;
    expiration: string;
    cvv: number;
    numberInstallments: number;
}