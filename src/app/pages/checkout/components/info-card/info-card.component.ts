import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.scss']
})
export class InfoCardComponent implements OnInit {
  @Input() number: string = '';
  @Input() name: string = '';
  @Input() expiration: string = '';
  @Input() cvv: string = '';
  @Input() typing : boolean = false;
  @Input() typingStatusCvv : boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
