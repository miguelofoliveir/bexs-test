import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Card } from '../models/checkout-interface';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  public webUrl = environment.application.api;

  constructor(private http: HttpClient) { }

  public sendDataPayment(body: Card) {
    return this.http.post(`${this.webUrl}/pagar`, body);
  }
}
