import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Card } from './models/checkout-interface';
import { CheckoutService } from './services/checkout.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  menuItems: string[] = ['Carrinho', 'Pagamento', 'Confirmação'];
  cvvEnable: boolean = false;
  numberStatus: boolean = false;

  public registerCard: FormGroup = this.f.group({
    number: new FormControl('', [Validators.minLength(16), Validators.maxLength(16)]),
    name: new FormControl('', [Validators.minLength(10), Validators.pattern(/[A-Z][a-z]* [A-Z][a-z]*/)]),
    expiration: new FormControl('', [Validators.minLength(4),this.validatorExpiration()]),
    cvv: new FormControl('', Validators.minLength(3)),
    numberInstallments: new FormControl('', Validators.required)
  });

  constructor(private f: FormBuilder, private checkoutService: CheckoutService) { }

  ngOnInit(): void {
  }

  private validatorExpiration(): ValidatorFn {
    return (control: AbstractControl) => {
      const month = +control.value.slice(0,2);
      const year = +control.value.slice(2,4);
      const currentYear = +(new Date()).getFullYear().toString().slice(2,4);
      const isValidMonth = month >= 1 && month <= 12;
      const isValidYear = year >= currentYear;

      return (isValidMonth && isValidYear) ? null : { isValidMonth: { value: control.value } }
    };
  }

  onFormSubmit(data: Card) {
    this.checkoutService.sendDataPayment(data).subscribe(
      res => { },
      err => { },
      () => { }
    )
  };

  cvvStatusOn() {
    this.cvvEnable = true;
  }

  cvvStatusOff() {
    this.cvvEnable = false;
  }

  checkInputNumber() {
    let numberCard = this.registerCard.get('number')?.value;
    if (numberCard.length > 1) {
      this.numberStatus = true;
    } else {
      this.numberStatus = false;
    }
  }

}
